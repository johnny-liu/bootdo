package com.bootdo.api.controller;

import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.R;
import com.bootdo.product.domain.ProductDO;
import com.bootdo.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api")
public class ApiController extends BaseController {
    @Autowired
    ProductService bProductService;
    @ResponseBody
    @RequestMapping(value="/product/addlist",method = RequestMethod.POST,produces = "application/json")
    public R addProduct(@RequestBody List<ProductDO> productDOS)
    {
        int count =bProductService.SaveOrUpdate(productDOS);
        if (count > 0) {
            return R.ok();
        }
        return R.error();
    }
}
