package com.bootdo.product.service.impl;

import com.bootdo.product.dao.ProductDao;
import com.bootdo.product.domain.ProductDO;
import com.bootdo.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDao productDao;
	
	@Override
	public ProductDO get(Long cid){
		return productDao.get(cid);
	}
	
	@Override
	public List<ProductDO> list(Map<String, Object> map){
		return productDao.list(map);
	}
	@Override
	public int count(Map<String, Object> map){
		return productDao.count(map);
	}
	
	@Override
	public int save(ProductDO bContent){
		return productDao.save(bContent);
	}

	@Override
	public int SaveOrUpdate(List<ProductDO> productDOList) {
		return productDao.SaveOrUpdate(productDOList);
	}
	@Override
	public int update(ProductDO bContent){
		return productDao.update(bContent);
	}
	
}
