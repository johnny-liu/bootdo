package com.bootdo.product.service;

import com.bootdo.product.domain.ProductDO;

import java.util.List;
import java.util.Map;

/**
 * 文章内容
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-09 10:03:34
 */
public interface ProductService {
	
	ProductDO get(Long cid);
	
	List<ProductDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);
	
	int save(ProductDO bContent);

	int SaveOrUpdate(List<ProductDO> productDOList);

	int update(ProductDO bContent);
}
