package com.bootdo.product.dao;

import com.bootdo.product.domain.ProductDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 文章内容
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-10-03 16:17:48
 */
@Mapper
public interface ProductDao {

	ProductDO get(Long id);
	
	List<ProductDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);
	
	int save(ProductDO content);


	int SaveOrUpdate(List<ProductDO> productDOList);
	
	int update(ProductDO content);
	

}
