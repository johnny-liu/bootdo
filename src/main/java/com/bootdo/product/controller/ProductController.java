package com.bootdo.product.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.bootdo.common.config.BootdoConfig;
import com.bootdo.common.config.Constant;
import com.bootdo.common.controller.BaseController;
import com.bootdo.common.utils.*;
import com.bootdo.product.domain.ProductDO;
import com.bootdo.product.service.ProductService;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 文章内容
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-09 10:03:34
 */
@Controller
@RequestMapping("/product")
public class ProductController extends BaseController {
	@Autowired
	ProductService bProductService;
	@Autowired
	BootdoConfig bootdoConfig;
	@GetMapping()
	@RequiresPermissions("product:bContent")
	String product() {
		return "product/bContent/bContent";
	}


	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("product:bContent")
	public PageUtils list(@RequestParam Map<String, Object> params) {
		Query query = new Query(params);
		List<ProductDO> bContentList = bProductService.list(query);
		int total = bProductService.count(query);
		PageUtils pageUtils = new PageUtils(bContentList, total);
		return pageUtils;
	}

	@GetMapping("/add")
	@RequiresPermissions("blog:bContent:add")
	String add() {
		return "blog/bContent/add";
	}

	@GetMapping("/edit/{cid}")
	@RequiresPermissions("blog:bContent:edit")
	String edit(@PathVariable("cid") Long cid, Model model) {
		ProductDO bProductDO = bProductService.get(cid);
		model.addAttribute("bContent", bProductDO);
		return "blog/bContent/edit";
	}

	@ResponseBody
	@RequiresPermissions("product:export")
    @RequestMapping("/export")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response,
							@RequestParam Map<String, Object> params) throws IOException {
		List<ProductDO> productDOList= bProductService.list(params);
		ArrayList<ProductDO> copyPDOList=new ArrayList<ProductDO>();
		int index=0;
        for (ProductDO products:productDOList) {
            if (!StringUtils.isEmpty(products.getImgUrl())) {
                String url=products.getImgUrl();
                String filePath = url.replace("/files/", "");
                filePath = bootdoConfig.getUploadPath() + filePath;
                File file = new File(filePath);
                if (file.exists()) {
                    productDOList.get(index).setImgUrl(filePath);
                    copyPDOList.add(productDOList.get(index));
                }
            }
            index++;
        }
		Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), ProductDO.class, copyPDOList);
        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition", "attachment;filename=createList.xls");//默认Excel名称
        response.flushBuffer();
        workbook.write(response.getOutputStream());
	}



}
