package com.bootdo.product.domain;

import cn.afterturn.easypoi.excel.annotation.Excel;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;


/**
 * 文章内容
 * 
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-22 13:16:10
 */
public class ProductDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//
	private Long id;
    @Excel(name="商品编号",orderNum = "0")
    private String proCode;
    @Excel(name="原厂编号",orderNum = "1")
    private String mfrCode;
	@Excel(name="品名",orderNum = "2")
	private String productName;
	@Excel(name="描述",orderNum = "3")
	private String productDesc;
    @Excel(name="供应商编号",orderNum = "4")
    private String supplierCode;
    @Excel(name="品牌",orderNum = "5")
    private String brand;

	private Date createdDate;

	private Date modifiedDate;
	@Excel(name="颜色",orderNum = "6")
	private String color;
	@Excel(name="尺码",orderNum = "7")
	private String size;

	private double soPrice;

	@Excel(name="poPrice",orderNum ="8")
	private double poPrice;

	@Excel(name="数量",orderNum = "9")
    private Integer stockQty;

    @Excel(name="货品位置",orderNum = "10")
    private String locator;

    @Excel(name="图片",type = 2,width = 20,height = 40,orderNum = "11")
    private String imgUrl;

    private String isActive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDesc() {
		return productDesc;
	}

	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public double getSoPrice() {
		return soPrice;
	}

	public void setSoPrice(double soPrice) {
		this.soPrice = soPrice;
	}

	public double getPoPrice() {
		return poPrice;
	}

	public void setPoPrice(double poPrice) {
		this.poPrice = poPrice;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

    public String getProCode() {
        return proCode;
    }

    public void setProCode(String proCode) {
        this.proCode = proCode;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getLocator() {
        return locator;
    }

    public void setLocator(String locator) {
        this.locator = locator;
    }

    public Integer getStockQty() {
        return stockQty;
    }

    public void setStockQty(Integer stockQty) {
        this.stockQty = stockQty;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
